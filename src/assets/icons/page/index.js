// 搜索框
import search from './search.svg?inline'

// 店铺公告
import volumeFill from './volume-fill.svg?inline'

export { search, volumeFill }
